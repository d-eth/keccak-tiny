# Keccak
Package implements keccak family functions

## Example
```d
import keccak;

auto data = cast(ubyte[]) "balanceOf(address)";

ubyte[256] hash1 = keccak256(data);

ubyte[256] hash2;
keccak_256(hash2.ptr, hash2.length, data.ptr, data.length);

ubyte[256] hash3;
keccakImpl!256(hash2[], source);

/// Could be also called in compile time!
static assert(keccak256(cast(ubyte[]) "hello")[0] == cast(ubyte) 28u);
```


## References
 - port of: https://github.com/IoTone/keccak-tiny 

## Building and Testing
```sh
dub run  keccak-tiny
dub test keccak-tiny
```


